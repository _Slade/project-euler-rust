trait Squareable {
    fn squared(&self) -> Self;
}

impl Squareable for i32 {
    fn squared(&self) -> i32 {
        *self * *self
    }
}

pub fn solve() -> Option<(i32, i32, i32)> {
    for a in 1..1000 {
        for b in 1..1000 {
            let c = 1000 - a - b;
            if c.squared() == a.squared() + b.squared() {
                return Some((a, b, c));
            }
        }
    }
    None
}
