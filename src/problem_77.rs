const TWO_PI: f64 = 2.0 * std::f64::consts::PI;

// Approximate the number of prime partitions of n
// These are asymptotic approximations - they don't get the right result (71)
// See https://oeis.org/A000607
#[allow(dead_code)]
fn a(n: f64) -> f64 {
    f64::exp(TWO_PI * f64::sqrt(n / (3f64 * f64::ln(n))))
}

fn a2(n: f64) -> f64 {
    //          1             ⎧     ⏐⎺⎺⎺⎺n⎺⎺⎺   ⎡    ln(ln(n))⎤
    // ────────────────── exp ⎨2π * ⏐ ─────── * ⎢1 + ─────────⎥
    // 2∜[3*ln(n)] * n³/⁴     ⎩     ⎷ 3*ln(n)   ⎣      ln(n)  ⎦
    let coeff = 1.0 / ((2.0 * (3.0 * n.ln())).powf(0.25) * n.powf(0.75));
    let lhs = TWO_PI * (n / (3.0 * n.ln())).sqrt();
    let rhs = 1.0 + n.ln().ln() / n.ln();
    coeff * (lhs * rhs).exp()
}

pub fn solve() -> Option<(i32, f64)> {
    for i in 2.. {
        let partitions = a2(i as f64);
        if partitions > 5000.0 {
            return Some((i, partitions));
        }
    }
    None
}
