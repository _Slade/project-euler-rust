// https://stackoverflow.com/a/41536521/2884483
fn digits(n: usize) -> Vec<usize> {
    fn digits_inner(n: usize, xs: &mut Vec<usize>) {
        if n >= 10 {
            digits_inner(n / 10, xs);
        }
        xs.push(n % 10);
    }
    let mut xs = Vec::new();
    digits_inner(n, &mut xs);
    xs
}

fn sum_square_digits(n: usize) -> usize {
    let digits = &digits(n);
    digits.iter().map(|digit| digit * digit).sum()
}

pub fn solve() -> usize {
    let mut result = 0;
    let target = 10_000_000;
    let size = (81f64 * (target as f64).log10()).ceil() as usize + 1;
    let mut checked: Vec<bool> = vec![false; size];

    for i in 1..size {
        let mut sequence = sum_square_digits(i);
        while sequence > i && sequence != 89 {
            sequence = sum_square_digits(sequence);
        }
        if checked[sequence] || sequence == 89 {
            result += 1;
            checked[i] = true;
        }
    }

    for i in size..=target {
        if checked[sum_square_digits(i) as usize] {
            result += 1;
        }
    }

    result
}
