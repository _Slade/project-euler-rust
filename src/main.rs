extern crate clap;
use clap::{App, Arg};

mod problem_77;
mod problem_9;
mod problem_92;

fn main() {
    let matches = App::new("Project Euler")
        .version("0.1.0")
        .about("Solves several Project Euler problems")
        .arg(
            Arg::with_name("problem")
                .short("p")
                .long("problem")
                .value_name("PROBLEM")
                .help("The number of the problem to solve")
                .validator(|s| {
                    s.parse::<i32>()
                        .map(|_| ())
                        .map_err(|_| "The problem number must be an integer".to_string())
                })
                .takes_value(true),
        )
        .get_matches();
    match matches.value_of("problem") {
        Some("9") => match problem_9::solve() {
            Some(triplet) => println!(
                "The Pythagorean triplet (a, b, c) satisfying a + b + c = 1000 is {:?}",
                triplet
            ),
            None => no_solution(),
        },
        Some("77") => match problem_77::solve() {
            Some((i, digits)) => println!(
                "The first number that can be partitioned into >=5,000 prime \
                 parts is {} with {:.0} parts",
                i, digits
            ),
            None => no_solution(),
        },
        Some("92") => println!("{} numbers arrive at 89", problem_92::solve()),
        Some(problem) => println!("Unknown problem number: {}", problem),
        None => println!("No problem specified"),
    }
}

fn no_solution() {
    println!("No solution found");
}
